##Radiation Leak

![radiationleak.png](https://bitbucket.org/repo/An7yp8/images/2028213616-radiationleak.png)

a simple clone along the lines of the game
'Boulderdash' written in BASIC for the Amstrad CPC.

**Installation**

You'll need a CPC emulator of some kind.  
I have tested it successfully on [CPCBox](http://www.cpcbox.com)- a browser based emulator written in javascript.

Load the .dsk file into drive A and type the following

```
	load 'radleak.bas'
	run
```

**Description**

Navigate 5 caves of radiation waste, treating each casket in each level whilst avoiding being crushed
or trapped by boulders.


**Controls**

Keyboard
```
  z - left , x - right
  p - up, l -down

  a - restart screen
  q - quit game
```
